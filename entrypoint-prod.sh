#!/bin/sh

if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres db..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "DB started"
fi

exec "$@"