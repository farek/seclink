FROM python:3.5-alpine

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN mkdir -p /opt/app

WORKDIR /opt/app

COPY seclink /opt/app

RUN apk update \
    && apk add --virtual build-deps gcc python3-dev musl-dev \
    && apk add postgresql-dev \
    && pip install psycopg2 \
    && apk add build-base libffi-dev libmagic

RUN pip install --upgrade pip
RUN pip install pipenv

COPY ./Pipfile /opt/app

RUN pipenv install --skip-lock --system --dev

RUN apk del build-base build-deps libffi-dev

COPY ./entrypoint.sh /usr/bin/entrypoint.sh

RUN addgroup -S appgroup && adduser -S appuser -G appgroup

RUN mkdir /opt/staticfiles && chown -R appuser:appgroup /opt/staticfiles \
    && mkdir /opt/media && chown -R appuser:appgroup /opt/media

RUN chmod +x /usr/bin/entrypoint.sh

USER appuser

ENTRYPOINT ["/usr/bin/entrypoint.sh"]