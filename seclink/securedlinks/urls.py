from django.contrib.auth.decorators import login_required
from django.urls import path
from securedlinks import views as links_views

app_name = 'securedlinks'

urlpatterns = [
    path('<uuid:link_slug>', links_views.AccessLink.as_view(), name='access-link'),
    path('create/', login_required(links_views.SecureLinkCreate.as_view()), name='create-link'),
    path('rest/create', login_required(links_views.create_link_rest), name='rest-create-link'),
    path('rest/link/<uuid:link_slug>', links_views.get_link_rest, name='rest-get-link'),
    path('rest/stats', login_required(links_views.get_statistics_rest), name='get-stats'),

]
