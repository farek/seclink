import magic
from rest_framework import serializers

from securedlinks.models import SecureLink


class SecureLinkSerializer(serializers.Serializer):
    url = serializers.URLField(allow_blank=False, required=False)
    file = serializers.FileField(required=False)

    class Meta:
        model = SecureLink
        fields = ('url', 'file')

    def update(self, instance, validated_data):
        raise NotImplementedError

    def create(self, validated_data):
        return SecureLink.objects.create(**validated_data)

    @staticmethod
    def validate_file(file):
        if file:
            filetype = magic.from_buffer(file.read())
            if not [s for s in ['xml', 'text', 'pdf'] if s in filetype]:
                raise serializers.ValidationError("File format is not supported!")
        return file

    def validate(self, data):
        if 'url' in data and 'file' in data:
            raise serializers.ValidationError("Provide ulr or file, not both!")
        if 'url' not in data and 'file' not in data:
            raise serializers.ValidationError("Provide ulr or file!")
        return data


class SecureLinkInfoSerializer(serializers.Serializer):
    slug = serializers.CharField()
    password = serializers.CharField()

    class Meta:
        model = SecureLink
        fields = ('slug', 'password')

    def update(self, instance, validated_data):
        raise NotImplementedError

    def create(self, validated_data):
        raise NotImplementedError


class PasswordSerializer(serializers.Serializer):
    password = serializers.CharField(max_length=64, min_length=8, required=True)

    class Meta:
        fields = 'password'

    def update(self, instance, validated_data):
        raise NotImplementedError

    def create(self, validated_data):
        raise NotImplementedError
