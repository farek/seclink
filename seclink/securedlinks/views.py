from django.http import HttpResponse, HttpResponseNotFound
from django.shortcuts import render, redirect
from django.utils import timezone
from django.views import View
from django.views.generic import FormView
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.decorators import api_view, parser_classes
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.request import Request
from rest_framework.response import Response

from securedlinks.forms import GetSecuredLinkForm, CreateSecureLinkForm
from securedlinks.models import SecureLink, AccessObject
from securedlinks.serializers import SecureLinkSerializer, SecureLinkInfoSerializer, PasswordSerializer


class AccessLink(View):
    template_name = 'get_link.html'
    http_method_names = ['get', 'post']

    def get(self, request, *args, **kwargs):
        uuid = kwargs.get('link_slug', None)
        try:
            link = SecureLink.objects.get(slug=uuid)
            if link.is_expired():
                link = None
        except SecureLink.DoesNotExist:
            link = None
        AccessObject.objects.create(link_type=AccessObject.TYPE_LINK if link.url else AccessObject.TYPE_FILE,
                                    link=link, success=False)
        return render(request, self.template_name, {
            'form': GetSecuredLinkForm(),
            'link': link
        })

    def post(self, request, *args, **kwargs):
        form = GetSecuredLinkForm(request.POST or None)
        uuid = kwargs.get('link_slug', None)
        try:
            link = SecureLink.objects.get(slug=uuid)
        except SecureLink.DoesNotExist:
            link = None
        if form.is_valid() and link:
            pas_input = form.cleaned_data.get('password', None)
            if pas_input == link.password:
                AccessObject.objects.create(link_type=AccessObject.TYPE_LINK if link.url else AccessObject.TYPE_FILE,
                                            link=link, success=True)
                if link.url:
                    return render(request, "show_link.html", {
                        'link': link
                    })
                else:
                    return prepare_file_attachment_response(link)
            else:
                form.add_error(field='password', error="Wrong password provided")
                return render(request, self.template_name, {
                    'form': form,
                    'link': link
                })
        return redirect('securedlinks:access-link' if uuid else '/', uuid)


class SecureLinkCreate(FormView):
    template_name = 'create_link.html'
    form_class = CreateSecureLinkForm
    object = None
    success_url = '/'

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        file = request.FILES.get('file', None)
        if form.is_valid():
            url = form.cleaned_data.get('url', None)
            link = SecureLink.objects.create(url=url, file=file)
            root = request.META.get('HTTP_ORIGIN', '')
            return render(self.request, 'new_link_details.html', {
                'link': link,
                'root_url': root
            })
        return self.form_invalid(form)


@swagger_auto_schema(method='post', request_body=SecureLinkSerializer, responses={201: SecureLinkInfoSerializer})
@api_view(['POST'])
@parser_classes([FormParser, MultiPartParser])
def create_link_rest(request: Request):
    if request.method == 'POST':
        serializer = SecureLinkSerializer(data=request.data)
        if serializer.is_valid():
            obj = serializer.save()
            return Response(SecureLinkInfoSerializer(obj).data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@swagger_auto_schema(method='post', request_body=PasswordSerializer, responses={200: 'link'})
@api_view(['POST'])
@parser_classes([FormParser, MultiPartParser])
def get_link_rest(request: Request, link_slug: str):
    if request.method == 'POST':
        serializer = PasswordSerializer(data=request.data)
        if serializer.is_valid():
            try:
                link = SecureLink.objects.get(slug=link_slug)
                if link.is_expired():
                    return Response("Link expired!", status=status.HTTP_400_BAD_REQUEST)
                AccessObject.objects.create(link_type=AccessObject.TYPE_LINK if link.url else AccessObject.TYPE_FILE,
                                            link=link, success=False)
            except SecureLink.DoesNotExist:
                return Response("No such link!", status=status.HTTP_400_BAD_REQUEST)
            if serializer.validated_data['password'] == link.password:
                AccessObject.objects.create(link_type=AccessObject.TYPE_LINK if link.url else AccessObject.TYPE_FILE,
                                            link=link, success=True)
                if link.url:
                    return Response({'link': link.url}, status=status.HTTP_200_OK)
                else:
                    return prepare_file_attachment_response(link)
            return Response("Bad password!", status=status.HTTP_403_FORBIDDEN)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def prepare_file_attachment_response(link):
    try:
        with open(link.file.path, 'r') as f:
            file_data = f.read()
        response = HttpResponse(file_data, content_type='text/plain')
        response['Content-Disposition'] = 'attachment; filename="{}"'.format(link.file_name)
    except IOError:
        response = HttpResponseNotFound('<h1>File does not exist</h1>')
    return response


@swagger_auto_schema(method='get', request_body=None, responses={200: 'list'})
@api_view(['GET'])
def get_statistics_rest(request: Request):
    if request.method == 'GET':
        result = {}
        access_objects = AccessObject.objects.all()
        for day in daterange(access_objects.first().access_date, access_objects.last().access_date):
            result.update({
                '{date}'.format(date=day.strftime("%Y-%m-%d")): {
                    'files': get_access_number(access_objects.filter(link__created_date__day=day.day,
                                                                     link__created_date__month=day.month,
                                                                     link__created_date__year=day.year),
                                               AccessObject.TYPE_FILE),
                    'links': get_access_number(access_objects.filter(link__created_date__day=day.day,
                                                                     link__created_date__month=day.month,
                                                                     link__created_date__year=day.year),
                                               AccessObject.TYPE_LINK),
                }
            })
        return Response(result, status=status.HTTP_200_OK)
    return Response({}, status=status.HTTP_400_BAD_REQUEST)


def get_access_number(objects, type):
    count = set()
    for obj in objects.filter(success=False):
        if obj.link_type == type:
            count.add(obj.link.id)
    return len(count)


def daterange(date1, date2):
    for n in range(int((date2 - date1).days)+1):
        yield date1 + timezone.timedelta(n)
