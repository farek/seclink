from django.contrib import admin, messages
from django.http import HttpResponseRedirect

from securedlinks.forms import SecureLinkPasswordForm
from securedlinks.models import SecureLink, AccessObject


@admin.register(SecureLink)
class SecureLinkAdmin(admin.ModelAdmin):
    list_display = ['slug', 'created_date']
    readonly_fields = ['created_date', 'file_name']
    exclude = ['password']
    change_form_template = "secured_link_change.html"
    password_form = None

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        if not self.password_form:
            self.password_form = SecureLinkPasswordForm()
        if 'password_form' not in extra_context:
            extra_context['password_form'] = self.password_form
        return super().change_view(request, object_id, form_url, extra_context=extra_context)

    def response_change(self, request, obj):
        if 'change-password' in request.POST:
            form = SecureLinkPasswordForm(request.POST or None)
            if form.is_valid():
                obj.password = form.cleaned_data['new_password']
                obj.save()
                messages.success(request, "Password changed!")
                self.password_form = None
            else:
                self.password_form = form
            return HttpResponseRedirect('.')
        return super().response_change(request, obj)


@admin.register(AccessObject)
class AccessObjectAdmin(admin.ModelAdmin):
    list_display = ['link_type', 'link', 'access_date', 'success']
    list_filter = ['link_type', 'access_date']
