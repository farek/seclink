import magic
from django import forms
from django.core.exceptions import ValidationError

from securedlinks.models import SecureLink


class SecureLinkPasswordForm(forms.Form):
    new_password = forms.CharField(widget=forms.PasswordInput(), label="New password", max_length=64)
    new_password_confirm = forms.CharField(widget=forms.PasswordInput(), label="Confirm password", max_length=64)

    field_order = ('new_password', 'new_password_confirm')

    def clean(self):
        if "change-password" in self.data:
            new_password = self.cleaned_data.get('new_password', '')
            new_password_confirm = self.cleaned_data.get('new_password_confirm', '')
            if new_password == new_password_confirm and len(new_password) > 7:
                return self.cleaned_data
            else:
                raise forms.ValidationError("Passwords should match and be at least 8 letters long!")


class CreateSecureLinkForm(forms.ModelForm):
    class Meta:
        model = SecureLink
        fields = ['url', 'file']

    def clean_file(self):
        file = self.cleaned_data.get("file", None)
        if file:
            filetype = magic.from_buffer(file.read())
            if not [s for s in ['xml', 'text', 'pdf'] if s in filetype]:
                raise ValidationError("File format is not supported!")
        return file


class GetSecuredLinkForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput(), label="Password", max_length=64)
