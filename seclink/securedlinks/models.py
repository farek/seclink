import uuid

from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone
from django.utils.crypto import get_random_string
from fernet_fields import EncryptedCharField


class SecureLink(models.Model):
    password = EncryptedCharField(max_length=64, blank=False, null=False)
    slug = models.UUIDField(default=uuid.uuid4, editable=False)
    url = models.URLField(null=True, blank=True, max_length=255)
    file = models.FileField(null=True, blank=True)
    file_name = models.CharField(null=True, blank=True, max_length=250)
    created_date = models.DateTimeField()

    def save(self, *args, **kwargs):
        if not self.created_date:
            self.created_date = timezone.now()
        if not self.password:
            self.password = get_random_string(15, 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)')
        if self.file:
            self.file_name = self.file.name
        super().save(*args, **kwargs)

    def clean(self):
        if (not self.file or not self.file.name) and (not self.url or self.url == ''):
            raise ValidationError('Url or File has to be filled!')
        if self.file and self.url:
            raise ValidationError('File URL or File, not both!')

    def __str__(self):
        return self.slug.__str__()

    def is_expired(self):
        return timezone.now() >= self.created_date + timezone.timedelta(hours=24)


class AccessObject(models.Model):
    TYPE_LINK = 'l'
    TYPE_FILE = 'f'
    TYPE_CHOICES = [
        (TYPE_LINK, 'Link'),
        (TYPE_FILE, 'File')
    ]
    link_type = models.CharField(choices=TYPE_CHOICES, max_length=1)
    link = models.ForeignKey(SecureLink, on_delete=models.SET_NULL, null=True)
    success = models.BooleanField(default=False)
    access_date = models.DateTimeField()

    def save(self, *args, **kwargs):
        if not self.access_date:
            self.access_date = timezone.now()
        super().save(*args, **kwargs)
