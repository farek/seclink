from unittest.mock import patch

from django.apps import apps
from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from rest_framework import status
from rest_framework.test import APITestCase

from securedlinks.apps import LinksConfig
from securedlinks.models import SecureLink


class LinksConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(LinksConfig.name, 'securedlinks')
        self.assertEqual(apps.get_app_config('securedlinks').name, 'securedlinks')


class SecureLinkCreateRest(APITestCase):
    URL = reverse('securedlinks:rest-create-link')

    def setUp(self):
        self.user = get_user_model().objects.create_user(username="username", password="password")

    def test_not_logged_redirected(self):
        response = self.client.post(self.URL, {})

        self.assertEqual(response.status_code, status.HTTP_302_FOUND)

    def test_bad_method(self):
        self.client.force_login(self.user)

        response = self.client.get(self.URL, {})

        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_empty_body(self):
        self.client.force_login(self.user)

        response = self.client.post(self.URL, {})

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_correct_body_with_link(self):
        self.client.force_login(self.user)

        response = self.client.post(self.URL, {'url': 'http://google.com'})

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class SecureLinkGetRest(APITestCase):
    fixtures = ['links.json']
    URL = reverse('securedlinks:rest-get-link', args=['b3c9d3a3-51f4-42ac-8caf-1e1e3edd834d'])

    def setUp(self):
        self.user = get_user_model().objects.create_user(username="username", password="password")

    def test_not_logged_ok(self):
        response = self.client.post(self.URL, {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_wrong_password(self):
        response = self.client.post(self.URL, {'password': 'foobarfoo'})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_wrong_slug(self):
        response = self.client.post(reverse('securedlinks:rest-get-link',
                                            args=['b3c9d3a3-51f4-42ac-8caf-1e1e3edddddd']),
                                    {'password': 'foobarfoo'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_password_ok(self):
        response = self.client.post(self.URL,{'password': 'password'})

        self.assertEqual(response.data['link'], "http://google.com")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @patch('django.utils.timezone.now')
    def test_link_expired(self, now_mock):
        now_mock.return_value = SecureLink.objects.get(id=1).created_date + timezone.timedelta(days=5)
        
        response = self.client.post(self.URL, {'password': 'password'})

        self.assertEqual(response.data, "Link expired!")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)