from django.contrib.auth.models import User
from django.db import models


class UserInfo(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    user_agent = models.TextField(blank=True, null=True)
    last_access = models.DateTimeField(blank=True, null=True)
