from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.core.signals import request_started
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.http import SimpleCookie
from django.utils import timezone

from userinfo.models import UserInfo


@receiver(post_save, sender=User)
def create_user_info(sender, **kwargs):  # pylint: disable=unused-argument
    user = kwargs["instance"]
    if kwargs["created"]:
        user_info = UserInfo()
        user_info.user = user
        user_info.save()


@receiver(request_started)
def request_started_handler(environ, **kwargs):  # pylint: disable=unused-argument
    user = None
    if environ.get('HTTP_COOKIE'):
        cookie = SimpleCookie()
        cookie.load(environ['HTTP_COOKIE'])

        session_cookie_name = settings.SESSION_COOKIE_NAME
        if session_cookie_name in cookie:
            session_id = cookie[session_cookie_name].value
            try:
                session = Session.objects.get(session_key=session_id)
            except Session.DoesNotExist:
                session = None

            if session:
                user_id = session.get_decoded().get('_auth_user_id')
                try:
                    user = get_user_model().objects.get(id=user_id)
                except Exception:  # pylint: disable=broad-except
                    user = None
    if user:
        user.userinfo.user_agent = environ.get('HTTP_USER_AGENT', "Not provided")
        user.userinfo.last_access = timezone.now()
        user.userinfo.save()
