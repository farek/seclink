from django.urls import path
from userinfo import views as user_info_views

urlpatterns = [
    path('register/', user_info_views.Register.as_view(), name='register'),
]
