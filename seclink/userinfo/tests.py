from django.contrib.auth import get_user_model
from django.test import TestCase, override_settings
from django.utils import timezone

from userinfo.apps import UserinfoConfig


@override_settings(USE_TZ=True)
class UserInfoTest(TestCase):

    def setUp(self):
        self.user = get_user_model().objects.create_user(username="username", password="password")
        self.user.userinfo.user_agent = "test_agent"
        self.user.userinfo.last_access = timezone.now() - timezone.timedelta(days=2)

    def test_app_config(self):
        self.assertEqual(UserinfoConfig.name, 'userinfo')

    def test_user_request_changes_user_agent_not_provided(self):
        self.client.force_login(self.user)
        self.client.get('/api')

        self.user.refresh_from_db()

        self.assertEqual(self.user.userinfo.user_agent, "Not provided")
        self.assertEqual(self.user.userinfo.last_access.day, timezone.now().day)

    def test_user_request_changes_user_agent_provided(self):
        self.client.force_login(self.user)
        header = {'HTTP_USER_AGENT': 'custom_agent'}
        self.client.get('/api', **header)

        self.user.refresh_from_db()

        self.assertEqual(self.user.userinfo.user_agent, "custom_agent")
        self.assertEqual(self.user.userinfo.last_access.day, timezone.now().day)

