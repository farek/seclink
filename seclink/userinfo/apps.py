from django.apps import AppConfig


class UserinfoConfig(AppConfig):
    name = 'userinfo'

    def ready(self):
        import userinfo.singals  # pylint: disable=unused-argument,unused-variable
