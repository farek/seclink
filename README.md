# seclink

This project provides two docker-compose files, one for development and one for production enviroment.  

To run test:  
```python manage.py test```

To prepare dev env:  
``docker-compose -f docker-compose.yml build``

Run dev env:  
``docker-compose -f docker-compose.yml up -d``

To prepare prod env:  
``docker-compose -f docker-compose.prod.yml build``

Run dev env:  
``docker-compose -f docker-compose.prod.yml up -d``

There is swagger endpoint for REST API testing available under ``/api`` endpoint.  

Auth:  
Login: ``auth/login``  
logout: ``auth/logout``  
admin: ``admin``  
register: ``register``  

URLs for accessing data on web:  
Access link: ``/link/<UUID>``  
Create link: ``/link/create``

REST endpoints:  
Create link: ``link/rest/create``  
Access link: ``link/rest/link/<UUID>``  
Statistics: ``link/rest/stats``  